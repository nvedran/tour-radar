//=require ./data.js
var App = function(name) {
    this.name     = name;
    var api = new HttpClient();
    //api.get("/_data/data.json");
    api.get("https://api.myjson.com/bins/18rdhq");
};

App.prototype.query = function(selector, all) {
    return document[all ? 'querySelectorAll' : 'querySelector'](selector);
};

App.prototype.on = function(e, callback, element) {
    (element ? element : document).addEventListener(e, callback);
};

App.prototype.fire = function(name, data) {
    var e = new CustomEvent(name, {detail: data});
    document.dispatchEvent(e);
};
