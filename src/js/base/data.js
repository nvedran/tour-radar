var HttpClient = function() {
    var self = this;

    this.httpClient = new XMLHttpRequest();
    this.httpClient.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            self.handleResponse(JSON.parse(this.responseText));
        }
    };

};

HttpClient.prototype.get = function(url) {
    this.httpClient.open("GET", url, true);
    this.httpClient.send();
};

HttpClient.prototype.handleResponse = function(data) {
    this.fire('data-ready', data);
};

HttpClient.prototype.fire = function(name, data) {
    var e = new CustomEvent(name, {detail: data});
    document.dispatchEvent(e);
};