// prepare filter object
var DataFilter = function(page) {
    this.page = page;
    this.data = page.data;
    this.results = page.results;
};

// add sort filter
DataFilter.prototype.sort = function(type) {

    this.results.sort(function (a, b) {
        switch (type) {
            case "reviews":
                return b.length - a.length;
            case "popularity":
                return b.rating - a.rating;
            case "asc-length":
                return a.length - b.length;
            case "desc-length":
                return b.length - a.length;
            case "asc-price":
                return a.dates[0].eur - b.dates[0].eur;
            case "desc-price":
                return b.dates[0].eur - a.dates[0].eur;
            default:
                return a.length - b.length;
        }

    });

    this.fire('render-data', this.results);
};

// add date filter
DataFilter.prototype.byDate = function(date) {
    // clone date array
    var tmp = this.data.filter(function () { return true; });

    var results = tmp.filter((function(item){
        // order dates
        item.dates = this.orderByDate(item.dates);

        var tmpDate = item.dates.filter(function(price){
            return price.start.search(date) >= 0 && price.availability;
        });

        item['latest'] = tmpDate;

        return tmpDate.length;
    }).bind(this));

    // render filtered results
    this.page.results = results;
    this.fire('render-data', results);
};

DataFilter.prototype.orderByDate = function(data, type) {
    data.sort(function(a, b) {

        // convert date to milliseconds
        var a = new Date(a.start),
            b = new Date(b.start);

        switch (type) {
            case "desc":
                return b.getTime() - a.getTime();
            default:
                return a.getTime() - b.getTime();
        }
    });

    return data;
};

// fire custom events
DataFilter.prototype.fire = function(name, data) {
    var e = new CustomEvent(name, {detail: data});
    document.dispatchEvent(e);
};