var LazyLoading = function (selector) {
    this.selector = selector;
    this.elements = Array.prototype.slice.call(document.querySelectorAll(selector));
    this._initLazyImages = this.initLazyImages.bind(this);

    window.addEventListener('scroll', this._initLazyImages, true);
};

LazyLoading.prototype.initLazyImages = function(){
    this.elements.forEach(function(item){
        var rect = item.getBoundingClientRect(), top = rect.top;
        if(top <= document.documentElement.clientHeight){
            var img = item.querySelector('img');
            if(img && !img.src) img.src = img.dataset.src;
        }
    });
    // remove scroll event after we load all images
    if(this.elements.length === document.querySelectorAll(this.selector + ' img[src]').length){
        window.removeEventListener('scroll', this._initLazyImages, true);
    }
};