//=require ./lazy.js

var TemplateManager = function(selector) {
    this.selector = selector;
};

TemplateManager.prototype.renderTemplate = function(id, data){
    var template = this.query(id).innerHTML;
    Mustache.parse(template);
    var container = this.query(this.selector);
    container.innerHTML = Mustache.render(template, {'data' :data});

    // init lazy loading
    var lazy = new LazyLoading('.data-list__item');
    lazy.initLazyImages();
}

TemplateManager.prototype.query = function(selector, all) {
    return document[all ? 'querySelectorAll' : 'querySelector'](selector);
};