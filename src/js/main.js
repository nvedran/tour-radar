(function(){
    //=require  ./base/app.js */
    //=require  ./components/filter.js */
    //=require  ./components/template.js */

    var page = new App('results', {});

    page.on('change', function(e){
        var filter = new DataFilter(page);
        filter.sort(e.target.value);
    }, page.query('.js-sort-filter'));

    page.on('change', function(e){
        var filter = new DataFilter(page);
        filter.byDate(e.target.value);
    }, page.query('.js-date-filter'));

    page.on('data-ready',function(e){
        page.data = e.detail;
        var filter = new DataFilter(page);
        filter.byDate();
    });

    page.on('render-data', function(e){

        // render date with Mustache js
        var template = new TemplateManager('.js-results-data');
        template.renderTemplate('#results', prepareData(e.detail));
    });


    // format date to keep frontend part more cleaner
    var prepareData = function(data) {
        data.forEach(function(item){
            item['content'] = {
                'image' : item['images'][0],
                'excerpt' : item.description.slice(0, 120) + '...',
                'start' : item['cities'][0],
                'end' : item['cities'][item['cities'].length - 1]
            };
            item['latest'] = item['latest'].slice(0, 2);
            item['latest'].forEach(function(item){
                item['formated_date'] = moment(item.start).format('D MMMM YYYY');
                item.availability = item.availability >10 ? '10+' : item.availability;
            });
        });
        return data;
    };

})();