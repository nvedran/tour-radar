var gulp = require("gulp");

var minify = require('gulp-minify');
var tap = require("gulp-tap");
var include = require("gulp-include");

var watch = require('gulp-watch');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');

gulp.task("compress", function() {

    return gulp
        .src("src/js/main.js")
        .pipe(tap(function(file) {
            return gulp
                .src(file.path)
                .pipe(include())
                .pipe(minify({
                    ext:{
                        src:'-debug.js',
                        min:'.min.js'
                    }
                }))
                .pipe(gulp.dest("dist/js"));
        }));

});



gulp.task('sass', function () {
    return gulp.src('./src/style/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer({ browsers: ['last 2 versions'] })]))
        .pipe(cleanCss())
        .pipe(rename({
            extname: ".min.css"
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('watch', ['sass'], function () {
    gulp.watch([
        './src/style/scss/*.scss',
        './src/style/scss/**/*.scss',
        '!./src/style/css/*.css',
    ], ['sass']).on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});