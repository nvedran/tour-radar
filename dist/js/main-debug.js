(function(){
    var HttpClient = function() {
        var self = this;
    
        this.httpClient = new XMLHttpRequest();
        this.httpClient.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                self.handleResponse(JSON.parse(this.responseText));
            }
        };
    
    };
    
    HttpClient.prototype.get = function(url) {
        this.httpClient.open("GET", url, true);
        this.httpClient.send();
    };
    
    HttpClient.prototype.handleResponse = function(data) {
        this.fire('data-ready', data);
    };
    
    HttpClient.prototype.fire = function(name, data) {
        var e = new CustomEvent(name, {detail: data});
        document.dispatchEvent(e);
    };
    var App = function(name) {
        this.name     = name;
        var api = new HttpClient();
        //api.get("/_data/data.json");
        api.get("https://api.myjson.com/bins/18rdhq");
    };
    
    App.prototype.query = function(selector, all) {
        return document[all ? 'querySelectorAll' : 'querySelector'](selector);
    };
    
    App.prototype.on = function(e, callback, element) {
        (element ? element : document).addEventListener(e, callback);
    };
    
    App.prototype.fire = function(name, data) {
        var e = new CustomEvent(name, {detail: data});
        document.dispatchEvent(e);
    };
    
    // prepare filter object
    var DataFilter = function(page) {
        this.page = page;
        this.data = page.data;
        this.results = page.results;
    };
    
    // add sort filter
    DataFilter.prototype.sort = function(type) {
    
        this.results.sort(function (a, b) {
            switch (type) {
                case "reviews":
                    return b.length - a.length;
                case "popularity":
                    return b.rating - a.rating;
                case "asc-length":
                    return a.length - b.length;
                case "desc-length":
                    return b.length - a.length;
                case "asc-price":
                    return a.dates[0].eur - b.dates[0].eur;
                case "desc-price":
                    return b.dates[0].eur - a.dates[0].eur;
                default:
                    return a.length - b.length;
            }
    
        });
    
        this.fire('render-data', this.results);
    };
    
    // add date filter
    DataFilter.prototype.byDate = function(date) {
        // clone date array
        var tmp = this.data.filter(function () { return true; });
    
        var results = tmp.filter((function(item){
            // order dates
            item.dates = this.orderByDate(item.dates);
    
            var tmpDate = item.dates.filter(function(price){
                return price.start.search(date) >= 0 && price.availability;
            });
    
            item['latest'] = tmpDate;
    
            return tmpDate.length;
        }).bind(this));
    
        // render filtered results
        this.page.results = results;
        this.fire('render-data', results);
    };
    
    DataFilter.prototype.orderByDate = function(data, type) {
        data.sort(function(a, b) {
    
            // convert date to milliseconds
            var a = new Date(a.start),
                b = new Date(b.start);
    
            switch (type) {
                case "desc":
                    return b.getTime() - a.getTime();
                default:
                    return a.getTime() - b.getTime();
            }
        });
    
        return data;
    };
    
    // fire custom events
    DataFilter.prototype.fire = function(name, data) {
        var e = new CustomEvent(name, {detail: data});
        document.dispatchEvent(e);
    };
    var LazyLoading = function (selector) {
        this.selector = selector;
        this.elements = Array.prototype.slice.call(document.querySelectorAll(selector));
        this._initLazyImages = this.initLazyImages.bind(this);
    
        window.addEventListener('scroll', this._initLazyImages, true);
    };
    
    LazyLoading.prototype.initLazyImages = function(){
        this.elements.forEach(function(item){
            var rect = item.getBoundingClientRect(), top = rect.top;
            if(top <= document.documentElement.clientHeight){
                var img = item.querySelector('img');
                if(img && !img.src) img.src = img.dataset.src;
            }
        });
        // remove scroll event after we load all images
        if(this.elements.length === document.querySelectorAll(this.selector + ' img[src]').length){
            window.removeEventListener('scroll', this._initLazyImages, true);
        }
    };
    
    var TemplateManager = function(selector) {
        this.selector = selector;
    };
    
    TemplateManager.prototype.renderTemplate = function(id, data){
        var template = this.query(id).innerHTML;
        Mustache.parse(template);
        var container = this.query(this.selector);
        container.innerHTML = Mustache.render(template, {'data' :data});
    
        // init lazy loading
        var lazy = new LazyLoading('.data-list__item');
        lazy.initLazyImages();
    }
    
    TemplateManager.prototype.query = function(selector, all) {
        return document[all ? 'querySelectorAll' : 'querySelector'](selector);
    };

    var page = new App('results', {});

    page.on('change', function(e){
        var filter = new DataFilter(page);
        filter.sort(e.target.value);
    }, page.query('.js-sort-filter'));

    page.on('change', function(e){
        var filter = new DataFilter(page);
        filter.byDate(e.target.value);
    }, page.query('.js-date-filter'));

    page.on('data-ready',function(e){
        page.data = e.detail;
        var filter = new DataFilter(page);
        filter.byDate();
    });

    page.on('render-data', function(e){

        // render date with Mustache js
        var template = new TemplateManager('.js-results-data');
        template.renderTemplate('#results', prepareData(e.detail));
    });


    // format date to keep frontend part more cleaner
    var prepareData = function(data) {
        data.forEach(function(item){
            item['content'] = {
                'image' : item['images'][0],
                'excerpt' : item.description.slice(0, 120) + '...',
                'start' : item['cities'][0],
                'end' : item['cities'][item['cities'].length - 1]
            };
            item['latest'] = item['latest'].slice(0, 2);
            item['latest'].forEach(function(item){
                item['formated_date'] = moment(item.start).format('D MMMM YYYY');
                item.availability = item.availability >10 ? '10+' : item.availability;
            });
        });
        return data;
    };

})();